create database exchangecurrency;

use exchangecurrency;

CREATE TABLE `orders` (
  `orderId` int(45) NOT NULL AUTO_INCREMENT,
  `userid` varchar(45) NOT NULL,
  `fromCur` varchar(45) DEFAULT NULL,
  `toCur` varchar(45) DEFAULT NULL,
  `amount` DOUBLE DEFAULT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  `originCountry` varchar(45) DEFAULT NULL,
  `processed` varchar(1) DEFAULT "N",
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
  
  
  CREATE TABLE `exchangecurrency`.`dashboard` (
  `orderid` INT NOT NULL,
  `userid` varchar(45) NULL,
  `fromcurrency` VARCHAR(45) NULL,
  `tocurrency` VARCHAR(45) NULL,
  `amount` DOUBLE NULL,
  `exchangerate` DOUBLE NULL,
  `exchangefee` DOUBLE NULL,
  `origincountry` VARCHAR(45) NULL,
  `timestamp` DATETIME NULL,
  PRIMARY KEY (`orderid`),
  CONSTRAINT `orderid`
    FOREIGN KEY (`orderid`)
    REFERENCES `exchangecurrency`.`orders` (`orderId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
  