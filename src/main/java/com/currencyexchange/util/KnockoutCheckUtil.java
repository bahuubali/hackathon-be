package com.currencyexchange.util;

import java.util.ArrayList;
import java.util.List;

import com.currencyexchange.constants.Constants;
import com.currencyexchange.dao.impl.KnockoutValidationDao;

public class KnockoutCheckUtil {

	public List<String> countryList = new ArrayList<String>();
	
	public boolean mortgageCheck(int existingMortgage) {
		if(existingMortgage == Constants.EXISTING_MORTGAGE_REFINANCING) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean creditScoreCheck(String pan) {
		KnockoutValidationDao knockoutVaidationDao = new KnockoutValidationDao();
		
		int creditScore = knockoutVaidationDao.checkCreditScore(pan);
		
		if(creditScore <= 7) {
			return true;
		} else 
			return false;
	}
	
	public void securityCheck() {
		//Stub to do securityCheck()
	}
	
	public boolean legalResidenceCheck(String city) {
		
		countryList.add("bangalore");
		countryList.add("mumbai");
		countryList.add("chennai");
		
		if(countryList.contains(city.toLowerCase())) {
			return false;
		} else {
			return true;
		}
	}
	
	public void salaryEligibilityCheck() {
		//Stub to do salaryEligibilityCheck()
	}
}
