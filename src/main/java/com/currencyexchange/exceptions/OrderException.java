package com.currencyexchange.exceptions;

public class OrderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8389109538349957485L;
	private String errorMessage;
	
	public OrderException() {
		super();
	}

	public OrderException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
}
