package com.currencyexchange.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.currencyexchange.model.ErrorResponse;


@ControllerAdvice
public class GlobalControllerExceptionHandler {

	@ExceptionHandler
	public ResponseEntity<ErrorResponse> customerNotFoundExceptionHandler(OrderException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorResponse.setMessage(ex.getErrorMessage());
		
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> customerDeleteExceptionHandler(CustomerDeleteException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorResponse.setMessage(ex.getErrorMessage());
		
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.OK);
	}
}
