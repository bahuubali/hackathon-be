package com.currencyexchange.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

public class OrderRowMapper implements RowMapper<OrderProcessingDto>{
	
	public OrderProcessingDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		OrderProcessingDto tempObj = new OrderProcessingDto();
		tempObj.setOrderId(rs.getInt("orderId"));
		tempObj.setUserId(rs.getString("userid"));
		tempObj.setFrom(rs.getString("fromCur"));
		tempObj.setTo(rs.getString("toCur"));
		tempObj.setToSell(rs.getString("amount"));
		tempObj.setTimePlaced(rs.getString("timestamp"));
		tempObj.setOriginCountry(rs.getString("originCountry"));
		tempObj.setProcessed(rs.getString("processed"));
		return tempObj;
	}

}
