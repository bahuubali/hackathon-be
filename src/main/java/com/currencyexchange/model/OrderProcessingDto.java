package com.currencyexchange.model;

public class OrderProcessingDto extends OrderDto {
	
	private int orderId;
	private String processed;
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getProcessed() {
		return processed;
	}
	public void setProcessed(String processed) {
		this.processed = processed;
	}
	
	

}
