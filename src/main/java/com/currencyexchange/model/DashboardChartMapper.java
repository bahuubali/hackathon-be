package com.currencyexchange.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

public class DashboardChartMapper implements RowMapper<DashboardChartDto>{
	
	public DashboardChartDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		DashboardChartDto tempObj = new DashboardChartDto();
		tempObj.setCurrency(rs.getString("fromCur"));
		tempObj.setTotalAmount(rs.getString("totamount"));		
		
		return tempObj;
	}

}
