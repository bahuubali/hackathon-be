package com.currencyexchange.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

public class DashboardRowMapper implements RowMapper<DashboardDto>{
	
	public DashboardDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		DashboardDto tempObj = new DashboardDto();
		tempObj.setOrderId(rs.getInt("orderid"));
		tempObj.setUserId(rs.getString("userid"));
		tempObj.setFrom(rs.getString("fromcurrency"));
		tempObj.setTo(rs.getString("tocurrency"));
		tempObj.setSell(rs.getDouble("amount"));
		tempObj.setRate(rs.getDouble("exchangerate"));
		tempObj.setBuy(rs.getDouble("exchangerate")*rs.getDouble("amount"));
		tempObj.setFee(rs.getDouble("exchangefee"));
		tempObj.setCountry(rs.getString("origincountry"));
		
		Timestamp tempTime = rs.getTimestamp("timestamp");		
		tempObj.setTime(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date(tempTime.getTime())));
		
		return tempObj;
	}

}
