package com.currencyexchange.service;

import com.currencyexchange.model.OrderDto;

public interface OrderService {

	OrderDto createOrder(OrderDto orderDto);
	
}
