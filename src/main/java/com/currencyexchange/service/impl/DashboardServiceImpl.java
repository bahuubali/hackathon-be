package com.currencyexchange.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.currencyexchange.dao.DashboardDAO;
import com.currencyexchange.dao.OrderDAO;
import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.DashboardDto;
import com.currencyexchange.service.DashboardService;

@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	DashboardDAO dashboardDao;

	@Override
	public List<DashboardDto> fetchDetails() {
		return dashboardDao.fetchDetails();
	}

	@Override
	public List<DashboardChartDto> fetchChartDetails() {
		return dashboardDao.fetchChartDetails();
	}

	
		

}
