package com.currencyexchange.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.currencyexchange.dao.OrderDAO;
import com.currencyexchange.model.OrderDto;
import com.currencyexchange.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderDAO orderDAO;

	@Override
	public OrderDto createOrder(OrderDto orderDto) {
		
		orderDAO.createOrder(orderDto);
		return orderDto;
	}
}
