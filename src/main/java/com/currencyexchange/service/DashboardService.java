package com.currencyexchange.service;

import java.util.List;

import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.DashboardDto;
import com.currencyexchange.model.OrderDto;

public interface DashboardService {

	List<DashboardDto> fetchDetails();
	
	List <DashboardChartDto> fetchChartDetails();
}
