package com.currencyexchange.constants;

import java.util.ArrayList;
import java.util.List;

public class Constants {
	
	public static final int NEW_MORTGAGE = 0;
	public static final int EXISTING_MORTGAGE = 1;
	public static final int EXISTING_MORTGAGE_REFINANCING = 2;
	
}
