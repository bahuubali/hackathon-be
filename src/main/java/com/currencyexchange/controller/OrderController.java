package com.currencyexchange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.currencyexchange.exceptions.OrderException;
import com.currencyexchange.model.OrderDto;
import com.currencyexchange.service.OrderService;

@RestController
@CrossOrigin
public class OrderController {

	@Autowired
	private OrderService oderService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/orders",
	produces = MediaType.APPLICATION_JSON_VALUE,
	consumes = MediaType.APPLICATION_JSON_VALUE,
	method = RequestMethod.POST
			)
	public ResponseEntity<OrderDto> createCustomer(@RequestBody OrderDto orderDto) throws OrderException {

		if(orderDto.getUserId() != null && orderDto.getUserId() != "" && 
				orderDto.getFrom() !=null && orderDto.getFrom() != "" && 
				orderDto.getTo() !=null && orderDto.getTo() != "" && 
				orderDto.getToSell() != null && orderDto.getToSell() != "" && 
				orderDto.getTimePlaced() !=null && orderDto.getTimePlaced() != "" && 
				orderDto.getOriginCountry() !=null && orderDto.getOriginCountry() != "") {
			
			oderService.createOrder(orderDto);
			return new ResponseEntity(orderDto, HttpStatus.OK);
		} else {
			throw new OrderException("All the fields are mandatory");
		}
	}
}
