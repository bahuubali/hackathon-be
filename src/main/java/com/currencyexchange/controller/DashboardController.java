package com.currencyexchange.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.DashboardDto;
import com.currencyexchange.service.DashboardService;

@RestController
@CrossOrigin
public class DashboardController {
	
	@Autowired
	private DashboardService dashboardService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/dashboard",
			produces = MediaType.APPLICATION_JSON_VALUE,			
			method = RequestMethod.GET
					)
	public ResponseEntity<List<DashboardDto>> fetchDetails() {		
		return new ResponseEntity(dashboardService.fetchDetails(), HttpStatus.OK);		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/dashboardChart",
			produces = MediaType.APPLICATION_JSON_VALUE,			
			method = RequestMethod.GET
					)
	public ResponseEntity<List<DashboardChartDto>> fetchChartDetails() {		
		return new ResponseEntity(dashboardService.fetchChartDetails(), HttpStatus.OK);		
	}
	
}
