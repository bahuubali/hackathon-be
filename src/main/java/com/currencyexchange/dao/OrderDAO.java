package com.currencyexchange.dao;

import java.util.List;

import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.OrderDto;
import com.currencyexchange.model.OrderProcessingDto;

public interface OrderDAO {

	int createOrder(OrderDto orderDto);
	
	List<OrderProcessingDto> fetchOrderDetailsForProcessing();
	
	
}
