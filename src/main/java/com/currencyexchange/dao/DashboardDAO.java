package com.currencyexchange.dao;

import java.util.List;

import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.DashboardDto;

public interface DashboardDAO {
	
	List<DashboardDto> fetchDetails();
	
	List<DashboardChartDto> fetchChartDetails();
}
