package com.currencyexchange.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.currencyexchange.dao.DashboardDAO;
import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.DashboardChartMapper;
import com.currencyexchange.model.DashboardDto;
import com.currencyexchange.model.DashboardRowMapper;

@Component
public class DashboardDAOImpl implements DashboardDAO {

	@Autowired
    JdbcTemplate jdbcTemplate;

	@Override
	public List<DashboardDto> fetchDetails() {
		String DASHBOARD_FETCH_SQL = "SELECT * from dashboard";
		
		List<DashboardDto> dashboardDtos = jdbcTemplate.query(DASHBOARD_FETCH_SQL,new DashboardRowMapper());		
		return dashboardDtos;
	}

	@Override
	public List<DashboardChartDto> fetchChartDetails() {
		String DASHBOARD_CHART_FETCH_SQL = "select distinct fromCurrency fromCur,sum(amount) totamount from dashboard group by fromCurrency";
		
		List<DashboardChartDto> dashBoardChartDtos = jdbcTemplate.query(DASHBOARD_CHART_FETCH_SQL,new DashboardChartMapper());		
		return dashBoardChartDtos;
	}
	
	
	
	
	
	

}
