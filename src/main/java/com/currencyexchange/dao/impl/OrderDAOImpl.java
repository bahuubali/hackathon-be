package com.currencyexchange.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.currencyexchange.dao.OrderDAO;
import com.currencyexchange.model.DashboardChartDto;
import com.currencyexchange.model.DashboardChartMapper;
import com.currencyexchange.model.OrderDto;
import com.currencyexchange.model.OrderProcessingDto;
import com.currencyexchange.model.OrderRowMapper;

@Component
public class OrderDAOImpl implements OrderDAO{

	@Autowired
    JdbcTemplate jdbcTemplate;

	@Override
	public int createOrder(OrderDto orderDto) {
		
		return jdbcTemplate.update("INSERT INTO orders(userid,fromCur,toCur,amount,timestamp,originCountry,processed) values (?,?,?,?,?,?,?)",
				orderDto.getUserId(),
				orderDto.getFrom(),
				orderDto.getTo(),
				orderDto.getToSell(),
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
				orderDto.getOriginCountry(),
				"N");
	}

	@Override
	public List<OrderProcessingDto> fetchOrderDetailsForProcessing() {
		String ORDER_PROCESSING_FETCH_SQL = "select * from orders where processed='N'";
		
		List<OrderProcessingDto> orderProcessingDtos = jdbcTemplate.query(ORDER_PROCESSING_FETCH_SQL,new OrderRowMapper());		
		return orderProcessingDtos;		
	}
	
	
	
}
