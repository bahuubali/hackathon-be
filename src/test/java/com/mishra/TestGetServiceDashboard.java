package com.mishra;

import static  io.restassured.RestAssured.*;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import generic.FWListener;
import generic.IAutoConst;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


import static org.hamcrest.Matchers.*;

@Listeners(FWListener.class)
public class TestGetServiceDashboard implements IAutoConst



{
	
	ExtentReports extent;
	ExtentTest logger;
	WebDriver driver;
	String driverPath = "./src/test/Drivers/";
	
	
	@BeforeTest
	public void startReport1()
	{
		
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
				extent
	            .addSystemInfo("Host Name", "Hackathon")
	            .addSystemInfo("Environment", "Automation Testing")
	            .addSystemInfo("User Name", "Girish Satyam");
	            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}
	
	/*
	 * Given Accept the Response in JSON Format
	 * When I Perform the POST request
	 * Then Status Code is 200 OK 
	 */

	@Test
	public static void testDasboard1() throws URISyntaxException
	{
		/*
		 * Given Accept the Response in JSON Format
		 * When I Perform the POST request
		 * Then Status Code is 200 OK 
		 */
		
		
		given()
		.contentType(ContentType.JSON)
		.get(new URI(ur1))
		.thenReturn()
		.getStatusCode();
		
	}
	
	
	@Test
	public void testGet() throws URISyntaxException
	{
		
		Response response = when().get(new URI(ur1));
		
		int code = response.getStatusCode();
		System.out.println("Status Code for testGet method is :-->>> "+ code );
		
		Assert.assertEquals(code, 200);
		
		String code1= response.asString();
		//System.out.println(code1);
		System.out.println("Response Time for testGet method is "+response.getTime());
		
		
	}
	
	@Test 
	public void testGet1() throws URISyntaxException
	{
		 Response resp1= given()
		.accept(ContentType.JSON)
		.when()
		.get(new URI(ur1));
		
		 System.out.println(resp1.asString());
		 System.out.println("Status Code for testGet1 method is  :-->> "+resp1.statusCode());
		 
		 Assert.assertEquals(HttpStatus.SC_OK, resp1.statusCode());
		
	}
	
	@Test
	public void testStatusCode() throws URISyntaxException
	{
		/*
		 * Given Accept the Response in JSON Format
		 * When I Perform the Get request
		 * Then Status Code is 200 OK 
		 * */
		int code3 = given()
		.accept(ContentType.JSON)
		.when()
		.get(new URI(ur1))
		.thenReturn() // thenReturn method is used for Capturing the parameters 
		.statusCode();		
		System.out.println("Status Code is testStatusCode method iz  -->> "+ code3);
		Assert.assertEquals(HttpStatus.SC_OK, code3);
		
		
	}
	

	// Testing the Body 
 
		@Test
		public void getHeader1() throws URISyntaxException
		{
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Accept", "application/json");
			
			String body =given()
			.headers(headers)
			.when()
			.get(new URI(ur1))
			.thenReturn()
			.asString();
			
			System.out.println("JSON Body for getHeader1 method is --> "+ body);
			
			
		}
		
	@Test
	public void testStatusCode1() throws URISyntaxException
	{
		/*
		 * Given Accept the Response in JSON Format
		 * When I Perform the Get request
		 * Then Status Code is 200 OK 
		 * */
		 given()
		.accept(ContentType.JSON)
		.when()
		.get(new URI(ur1))
		.then()  // then method is used for Validation Purpose 
		.assertThat()
		.statusCode(HttpStatus.SC_OK); 
		 
		 // then() for Validating 
		 // thenReturns -- For capturing 
		 
		 

		
		
	}
	
	
	@AfterMethod
	public void getResult(ITestResult result){
		if(result.getStatus() == ITestResult.FAILURE){
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
		}else if(result.getStatus() == ITestResult.SKIP){
			logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
		}
		// ending test
		//endTest(logger) : It ends the current test and prepares to create HTML report
		extent.endTest(logger);
		
	}
	@AfterTest
	public void endReport(){
		// writing everything to document
		//flush() - to write or update test information to your report. 
                extent.flush();
                extent.close();
    }
}
	
	
	

