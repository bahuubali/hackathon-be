package com.mishra;

import org.apache.http.HttpStatus;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import generic.FWListener;
import generic.IAutoConst;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

import java.io.File;

@Listeners(FWListener.class)
public class TestPostServiceOrders implements IAutoConst

{
	
	ExtentReports extent;
	ExtentTest logger;
	WebDriver driver;
	String driverPath = "./src/test/Drivers/";
	
	
	@BeforeTest
	public void startReport1()
	{
		
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMPostExtentReport.html", true);
				extent
	            .addSystemInfo("Host Name", "Hackathon")
	            .addSystemInfo("Environment", "Automation Testing")
	            .addSystemInfo("User Name", "Girish Satyam");
	            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}
	
	/*
	 * Given Accept the Response in JSON Format
	 * When I Perform the POST request
	 * Then Status Code is 200 OK 
	 */

		///TC 01 :- Verify that the user Enters all the mandatory value for POST Service(Orders). 
	
		@Test(priority=1)
		public void TestAddUser()
		{

			
			String  userCredential =TC01;
	    	//RestAssured.baseURI  = "http://10.117.189.29:8080/CurrencyExchange/orders";
			RestAssured.baseURI  = baseURI1;
	    	Response r = RestAssured.given()
	    				.contentType("application/json")
	    				.body(userCredential)
	    				.when().post("");
	      
	    	
	    	//validate status code
	    	//Assert.assertEquals(200, expected);
	    	System.out.println("Status Code for TestAddUser method is  -->  "+r.getStatusCode());
	    	Assert.assertEquals(HttpStatus.SC_OK, r.getStatusCode()); 
	    	logger = extent.startTest("passTest");
			Assert.assertTrue(true);
			//To generate the log when the test case is passed
			logger.log(LogStatus.PASS, "Test Case Passed is passTest");
			
	}
		
		///TC 02 :- Verify that for POST Service(Orders) --> All the fields (userid; from; to; tosell; timePlaced & originCountry) are manadatory.
		
			@Test(priority=2)
			public void test02()
			{
				
		 
				
				String  userCredential =TC02;
		    	RestAssured.baseURI  = baseURI1;	
		    	Response r = RestAssured.given()
		    				.contentType("application/json")
		    				.body(userCredential)
		    				.when().post("");
		      
		    	
		    	//validate status code
		    	//Assert.assertEquals(200, expected);
		    	System.out.println("Status Code for test02 -->  "+r.getStatusCode());
		    	Assert.assertEquals(HttpStatus.SC_OK, r.getStatusCode()); 
		    	logger = extent.startTest("passTest");
				Assert.assertTrue(true);
				//To generate the log when the test case is passed
				logger.log(LogStatus.PASS, "Test Case Passed is passTest");
				
		}
		
			///TC 03 :- Verify that the POST Service throws ; if "user id" field is left Blank & rest all fields have been passed with the Value.

			
			@Test(priority=3)
			public void test03()
			{
				
		 
				
				String  userCredential =TC03;
		    	RestAssured.baseURI  = baseURI1;	
		    	Response r = RestAssured.given()
		    				.contentType("application/json")
		    				.body(userCredential)
		    				.when().post("");
		      
		    	
		    	//validate status code
		    	//Assert.assertEquals(200, expected);
		    	System.out.println("Status Code for test05 -->  "+r.getStatusCode());
		    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
		    	logger = extent.startTest("failTest");
				Assert.assertTrue(true);
				// To generate the Log when the Test is Failed . 
				logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
				
		}
			
			///TC 04 :- Verify that the POST Service throws ; if "from" field is left Blank & rest all fields have been passed with the Value.


			
			@Test(priority=4)
			public void test04()
			{
				

				 
				
				String  userCredential =TC04;
		    	RestAssured.baseURI  = baseURI1;	
		    	Response r = RestAssured.given()
		    				.contentType("application/json")
		    				.body(userCredential)
		    				.when().post("");
		      
		    	
		    	//validate status code
		    	//Assert.assertEquals(200, expected);
		    	System.out.println("Status Code for test05 -->  "+r.getStatusCode());
		    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
		    	logger = extent.startTest("failTest");
				Assert.assertTrue(true);
				// To generate the Log when the Test is Failed . 
				logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
				
		}
		
			///TC 05 :- Verify that the POST Service throws ; if "to" field is left Blank & rest all fields have been passed with the Value.



			
			@Test(priority=5)
			public void test05()
			{
				

				 
				
				String  userCredential =TC05;
		    	RestAssured.baseURI  = baseURI1;	
		    	Response r = RestAssured.given()
		    				.contentType("application/json")
		    				.body(userCredential)
		    				.when().post("");
		      
		    	
		    	//validate status code
		    	//Assert.assertEquals(200, expected);
		    	System.out.println("Status Code for test05 -->  "+r.getStatusCode());
		    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
		    	logger = extent.startTest("failTest");
				Assert.assertTrue(true);
				// To generate the Log when the Test is Failed . 
				logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
				
		}
			
			
			///TC 06 :- Verify that the POST Service throws ; if "toSell" field is left Blank & rest all fields have been passed with the Value.



			
			@Test(priority=6)
			public void test06()
			{
				
		 
				
				String  userCredential =TC06;
		    	RestAssured.baseURI  = baseURI1;	
		    	Response r = RestAssured.given()
		    				.contentType("application/json")
		    				.body(userCredential)
		    				.when().post("");
		      
		    	
		    	//validate status code
		    	//Assert.assertEquals(200, expected);
		    	System.out.println("Status Code for test06 -->  "+r.getStatusCode());
		    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
		    	logger = extent.startTest("failTest");
				Assert.assertTrue(true);
				// To generate the Log when the Test is Failed . 
				logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
				
		}
			
			
			
		// TC 07 :-- Verify that the POST Service throws ; if "timePlaced" field is left Blank & rest all fields have been passed with the Value.
			
			@Test(priority=7)
			public void test07()
			{
				
	 
				
				String  userCredential =TC07;
		    	RestAssured.baseURI  = baseURI1;	
		    	Response r = RestAssured.given()
		    				.contentType("application/json")
		    				.body(userCredential)
		    				.when().post("");
		      
		    	
		    	//validate status code
		    	//Assert.assertEquals(200, expected);
		    	System.out.println("Status Code for test07 -->  "+r.getStatusCode());
		    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
		    	logger = extent.startTest("failTest");
				Assert.assertTrue(true);
				// To generate the Log when the Test is Failed . 
				logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
				
		}
	
			
			
			// TC 08 :-- Verify that the POST Service throws ; if "origincountry" field is left Blank & rest all fields have been passed with the Value.

			
						@Test(priority=8)
						public void test08()
						{
							
				 
							
							String  userCredential =TC08;
					    	RestAssured.baseURI  = baseURI1;	
					    	Response r = RestAssured.given()
					    				.contentType("application/json")
					    				.body(userCredential)
					    				.when().post("");
					      
					    	
					    	//validate status code
					    	//Assert.assertEquals(200, expected);
					    	System.out.println("Status Code for test08 method iz  -->  "+r.getStatusCode());
					    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
					    	logger = extent.startTest("failTest");
							Assert.assertTrue(true);
							// To generate the Log when the Test is Failed . 
							logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
							
					}
						
			// TC 09 :-- Verify that the POST Service throws ;  if  the user doesn�t pass any value into the  (userid; from; to; tosell; timePlaced & originCountry) fields. 
						
						@Test(priority=9)
						public void test09()
						{
							
				 
							
							String  userCredential =TC09;
					    	RestAssured.baseURI  = baseURI1;	
					    	Response r = RestAssured.given()
					    				.contentType("application/json")
					    				.body(userCredential)
					    				.when().post("");
					      
					    	
					    	//validate status code
					    	//Assert.assertEquals(200, expected);
					    	System.out.println("Status Code for test09 method  -->  "+r.getStatusCode());
					    	Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, r.getStatusCode()); 
					    	logger = extent.startTest("failTest");
							Assert.assertTrue(true);
							// To generate the Log when the Test is Failed . 
							logger.log(LogStatus.PASS, "Test Case (failTest) Status is passed");
							
					}
						
						
		
		@AfterMethod
		public void getResult(ITestResult result){
			if(result.getStatus() == ITestResult.FAILURE){
				logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
				logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
			}else if(result.getStatus() == ITestResult.SKIP){
				logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
			}
			// ending test
			//endTest(logger) : It ends the current test and prepares to create HTML report
			extent.endTest(logger);
			
		}
		@AfterTest
		public void endReport(){
			// writing everything to document
			//flush() - to write or update test information to your report. 
	                extent.flush();
	                extent.close();
	    }
	}
		
		
		
