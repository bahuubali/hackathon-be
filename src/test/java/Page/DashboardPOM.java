package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import generic.AutoUtility;

public class DashboardPOM extends AutoUtility
{



	@FindBy(xpath="username")
	private WebElement usernameTXB;
	
	
	

	public DashboardPOM(WebDriver driver) 
	{
		super(driver);
		
	}
	
	public void setUsername(String un)
	{
		log.info("Entering Username :--> " +un);
		usernameTXB.sendKeys(un);
		
	}
	

}
