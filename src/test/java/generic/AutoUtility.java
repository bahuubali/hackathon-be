package generic;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class AutoUtility 

{
	
	public WebDriver driver;
	public long lngETO;
	public Logger log;
	public WebDriverWait wait;
	String CONFIG_PATH;
	
	
	
	
	// Capturing the Screenshot by Using Takes Screenshot Method 
	public static void getScreenShot(WebDriver driver, String path)
	{
	
	// To get Screenshots of Page via Selenium Iterfaces 
	try 
	{
		TakesScreenshot t = (TakesScreenshot)driver; // type casting 
		File image = t.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(image, new File (path));
	}
	catch(Exception e )
	{
		
	}
	
	
	}
	
	public static String getPropertyValue(String path, String key)
	{
		String value = "";
		try 
		{
			Properties p = new Properties();
			p.load(new FileInputStream(path));
			value = p.getProperty(key);
		}
		catch (Exception e)
		{
			
		}
		return value;
	}
	
	
	// Capturing the Screenshot by Robot method 
	public static void getScreenShot(String path) 
	{
		try 
		{
			Robot r = new Robot();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle screenRect = new Rectangle(screenSize);
			BufferedImage image = r.createScreenCapture(screenRect);
			ImageIO.write(image, "png", new File(path));
		}
		catch (Exception e)
		{
			
		}
		
		
	}
	
	//
	public AutoUtility(WebDriver driver)
	{
		this.driver= driver;
		
		String strETO = AutoUtility.getPropertyValue(CONFIG_PATH, "ETO");
		lngETO = Long.parseLong(strETO);
		wait = new WebDriverWait(driver, lngETO);
		log = Logger.getLogger(this.getClass());
		PageFactory.initElements(driver,this); //Now onwards no need to write this in all POM classes
					
	}
	
	//Method for TimeStamp
	public static String now()
	{
		SimpleDateFormat s = new SimpleDateFormat("dd_MM_yy_hh_mm_ss");
		String timeStamp = s.format(new Date());
		return timeStamp; 
	}
	
	// Method for wait
	public static void sleep (int seconds)
	{
		try {
			Thread.sleep(seconds*1000);
		} 
		catch (InterruptedException e) 
		{
			
			e.printStackTrace();
		}
	}


}
