package generic;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.log4testng.Logger;

public class FWListener implements ITestListener , IAutoConst
{
	public Logger log = Logger.getLogger(this.getClass());
	
	// Capturing the Screenshot 
		public static void getScreenShot(WebDriver driver, String path)
		{
		
		// To get Screenshots of Page via Selenium Iterfaces 
		try 
		{
			TakesScreenshot t = (TakesScreenshot)driver; // type casting 
			File image = t.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(image, new File (path));
		}
		catch(Exception e )
		{
			
		}
		
		
		}
	

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSuccess(ITestResult result) 
	{
		String fileName=AutoUtility.now();
		String photopath= SCREENSHOT_PATH+fileName+".png";
		AutoUtility.getScreenShot(photopath);
		log.info("Screen Shot Taken & Stored  "+photopath);
		
	}

	@Override
	public void onTestFailure(ITestResult result)
	{
		String fileName=AutoUtility.now();
		String photopath= SCREENSHOT_PATH+fileName+".png";
		AutoUtility.getScreenShot(photopath);
		log.info("Screen Shot Taken & Stored  "+photopath);
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	

}
