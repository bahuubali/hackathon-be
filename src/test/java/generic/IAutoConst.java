package generic;

public interface IAutoConst 
{
	String CHROME_KEY = "webdriver.chrome.driver";
	String CHROME_VALUE = "./drivers/chromedriver.exe";
	
	String SCREENSHOT_PATH = "./src/test/screenshots/";
	
	
	
	String CONFIG_PATH = "./src/test/config.properties";
	
	//String baseURI1  = "http://10.117.189.29:8080/CurrencyExchange/orders";
	String baseURI1  = "http://34.238.84.54:8080/CurrencyExchange/orders";
	
	String TC01 = "{\"userId\":\"134256\",\"from\":\"EUR\",\"to\":\"GBP\",\"toSell\":\"6000\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"ES\"}";
	String TC02 = "{\"userId\":\"134256\",\"from\":\"EUR\",\"to\":\"GBP\",\"toSell\":\"6000\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"ES\"}";
	String TC03 = "{\"userId\":\"\",\"from\":\"EUR\",\"to\":\"GBP\",\"toSell\":\"6000\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"ES\"}";
	String TC04 = "{\"userId\":\"134256\",\"from\":\"\",\"to\":\"GBP\",\"toSell\":\"6000\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"ES\"}";
	String TC05 = "{\"userId\":\"134256\",\"from\":\"EUR\",\"to\":\"\",\"toSell\":\"6000\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"ES\"}";
	String TC06 = "{\"userId\":\"134256\",\"from\":\"EUR\",\"to\":\"GBP\",\"toSell\":\"\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"ES\"}";
	String TC07 = "{\"userId\":\"134256\",\"from\":\"EUR\",\"to\":\"GBP\",\"toSell\":\"6000\",\"timePlaced\":\"\",\"originCountry\":\"ES\"}";
	String TC08 = "{\"userId\":\"134256\",\"from\":\"EUR\",\"to\":\"GBP\",\"toSell\":\"6000\",\"timePlaced\":\"19-11-2017 10:27:44\",\"originCountry\":\"\"}";
	String TC09 = "{\"userId\":\"\",\"from\":\"\",\"to\":\"\",\"toSell\":\"\",\"timePlaced\":\"\",\"originCountry\":\"\"}";
	
	
	
	//Url for Get API
	String ur1= "http://34.238.84.54:8080/CurrencyExchange/dashboard";
	

}
